import { CronJob } from 'cron'

import {
  cv,
  cijs,
  // executeAllCoingeckoOperations,
  cmcMarketToSQL,
  // cmcToMongoDB,
} from './lib'

console.info(`
\t\tSTART
\t${new Date().toISOString()}
`)

cv.nodeEnvIsDev && cijs(cv)

/**
 * cg
 */

// setInterval(async () => {
//   await executeAllCoingeckoOperations()
// }, cv.cgTimeout)

/**
 * cmc
 */

cv.cmcToggleSQL &&
  new CronJob(
    cv.cmcCron,
    async () => {
      await cmcMarketToSQL()
    },
    null,
    true
  )

// cv.cmcToggleMongoDB && cmcToMongoDB()
