// import { MongoClient } from 'mongodb'

import * as cv from './config' // config variables
export { cv }

export const trackingNumber = {
  cgFav: 0,
  cgMarket: 0,
  cmc: 0,
}

export const cgFavcoinsLastTimestamps: Record<string, number> = {}
export let cgMarketLastTimestamp: number = 0
export const cmcLastTimestamp: number = 0

export const cgProps = [
  // 'id',
  'symbol',
  'name',
  'current_price',
  'market_cap',
  'market_cap_rank',
  'total_volume',
  'circulating_supply',
  'last_updated',
]

export const cgDataIsNew = (lastSaved: any, fromCloud: any) => {}

export const cijs = (obj: any) => console.info(JSON.stringify(obj, null, 2))

// export const errTracker = []
export const handleError = (messages: string) => {
  console.error(`${new Date().toISOString()}\n${messages}`)
  // throw new Error(messages)
}

export const isArrayWithData = (input: any) => {
  if (!Array.isArray(input)) {
    // cv.nodeEnvIsDev && console.info(`not Array`)
    return false
  }

  if (input?.length > 0) {
    cv.nodeEnvIsDev && console.info(`is Array, length: ${input?.length}`)
    return true
  } else {
    cv.nodeEnvIsDev && console.info(`is Array, empty`)
    return false
  }
}

export const isObjectWithData = (pr: any) => {
  if (!(pr !== null && typeof pr === 'object')) {
    // cv.nodeEnvIsDev && console.info(`not Object`)
    return false
  }

  if (Object.keys(pr).length > 0) {
    cv.nodeEnvIsDev && console.info(`isObject, has entries`)
    return true
  } else {
    cv.nodeEnvIsDev && console.info(`isObject, empty`)
    return false
  }
}

export const peekArrayOrObject = (pr: any) => {
  if (!isArrayWithData(pr) && !isObjectWithData(pr)) {
    cv.nodeEnvIsDev && console.info(`not Array nor Object`)
    return false
  } else {
    return true
  }
}

export const undiciFetchNoCache = async (
  path: string | string[],
  headers?: HeadersInit
) => {
  path = Array.isArray(path) ? path.join('') : path
  headers = {
    ...headers,
    accept: 'application/json',
    pragma: 'no-cache',
    'cache-control': 'no-cache',
  }

  const response = await fetch(path, {
    method: 'GET',
    headers,
    signal: AbortSignal.timeout(cv.undiciFetchTimeout),
  })

  if (!response) {
    handleError(`No response!`)
    return
  } else if (!response.ok) {
    handleError(
      `Network response was not OK: ${response.status} - ${response.statusText}`
    )
    return
  }

  return response
}

export const parseObjFromRes = async (response: Response) => {
  const contentType = response.headers.get('content-type')
  if (!contentType || !contentType.includes('application/json')) {
    handleError(`TypeError. Expect application/json. Got ${contentType} `)
    return null
  } else {
    return await response.json()
    // Note that despite the method being named json(), the result is not JSON but is instead the result of taking JSON as input and parsing it to produce a JavaScript object.
  }
}

export async function fetchThenParse(
  path: string | string[],
  headers?: HeadersInit
) {
  try {
    const response = await undiciFetchNoCache(path, headers)
    return await parseObjFromRes(response ?? new Response(undefined))
  } catch (error) {
    handleError(`undici fetch/parse error: ${error}`)
    return null
  }
}

export async function insertToDirectus(collection: string, data: any) {
  fetch(`${cv.directusBaseUrl}/items/${collection}`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${cv.directusToken}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
    signal: AbortSignal.timeout(cv.undiciFetchTimeout),
  })
    .then(async (response) => {
      console.info(`POST status: ${response.status}`)
      return
    })
    .catch((error) => {
      handleError(`undiciFetch POST error: ${error}`)
      return
    })
}

/**
 * CG
 */

// export const cgFavoriteCoinsToSQL = async () => {
//   console.info(`cgFav:${++trackingNumber.cgFav} - ${new Date().toISOString()}`)

//   const responseParsed = await fetchThenParse([
//     cv.cgApiBaseUrl,
//     `/simple/price`,
//     // `?ids=bitcoin`,
//     // `?ids=bitcoin,ethereum,solana`,
//     `?ids=${gc.cgCoinIdJoined}`,
//     `&vs_currencies=usd`,
//     `&include_market_cap=true`,
//     `&include_24hr_vol=true`,
//     `&include_last_updated_at=true`,
//     `&precision=full`,
//   ])

//   if (!peekArrayOrObject(responseParsed)) {
//     return
//   }

//   const newData = []

//   for (const symbol of gc.coinSymbols) {
//     const cgID = gc.favCoins[symbol as 'ada'].cg_id
//     cgFavcoinsLastTimestamps[cgID] ??= 0
//     let coinObj = responseParsed[cgID]
//     const cloudLastTimestamp = coinObj?.last_updated_at

//     const devLogOnlyBitcoin = cv.nodeEnvIsDev && symbol === 'btc'

//     devLogOnlyBitcoin &&
//       console.info(
//         `${cgID}: ${cgFavcoinsLastTimestamps[cgID]} - ${cloudLastTimestamp} = ${
//           cgFavcoinsLastTimestamps[cgID] - cloudLastTimestamp
//         }s`
//       )

//     if (cgFavcoinsLastTimestamps[cgID] < cloudLastTimestamp) {
//       coinObj = {
//         symbol,
//         name: cgID,
//         ...coinObj,
//       }

//       // convert some prop to BigInt
//       for (const _prop of ['usd_market_cap', 'usd_24h_vol']) {
//         coinObj[_prop] = Math.floor(coinObj[_prop])
//       }

//       devLogOnlyBitcoin && cijs(coinObj)
//       devLogOnlyBitcoin && console.info(new Date(cloudLastTimestamp * 1000))

//       newData.push(coinObj)
//       cgFavcoinsLastTimestamps[cgID] = cloudLastTimestamp
//     }
//   }

//   if (newData.length > 0) {
//     await insertToDirectus(cv.directusCgFavCollectionName, newData)
//   }
// }

export async function fetchCgMarketCoins() {
  let marketCoinsAllPage: any[] = []

  for (let index = 1; index <= cv.cgMarketPages; index++) {
    console.info(`cgMarket:${++trackingNumber.cgMarket}`)

    const marketCoinsEachPage = await fetchThenParse([
      cv.cgApiBaseUrl,
      `/coins/markets`,
      `?vs_currency=usd`,
      `&order=market_cap_desc`,
      `&per_page=${cv.cgMarketCoinsPerPage}`,
      `&page=${index}`,
      `&locale=en`,
    ]).catch((error) => {
      handleError(`fetchCgMarketCoins error: ${error}`)
      return []
    })

    if (!peekArrayOrObject(marketCoinsEachPage)) {
      return []
    }

    if (Array.isArray(marketCoinsEachPage) && marketCoinsEachPage.length > 0) {
      marketCoinsAllPage = [...marketCoinsAllPage, ...marketCoinsEachPage]
    } else {
      return []
    }
  }

  cv.nodeEnvIsDev && cijs(marketCoinsAllPage.at(-1))

  return marketCoinsAllPage
}

export async function cgMarketCoinsToSQL() {
  const marketCoins = await fetchCgMarketCoins()

  if (marketCoins.length > 0) {
    const redactedData: any[] = marketCoins?.map((_coinObj: any) => {
      return {
        symbol: _coinObj.symbol,
        name: _coinObj.name,
        rank: _coinObj.market_cap_rank,
        circulating_supply: Math.floor(_coinObj.circulating_supply),
        usd_price: _coinObj.current_price,
        usd_volume_24h: Math.floor(_coinObj.total_volume),
        usd_market_cap: Math.floor(_coinObj.market_cap),
        last_updated_str: _coinObj.last_updated,
        last_updated_unix: Math.floor(+new Date(_coinObj.last_updated) / 1000),
      }
    })

    const bottomCoin = redactedData.at(-1)
    cv.nodeEnvIsDev && cijs(bottomCoin)
    const cloudTimestamp = bottomCoin.last_updated_unix

    if (cloudTimestamp > cgMarketLastTimestamp) {
      cgMarketLastTimestamp = cloudTimestamp
      await insertToDirectus(cv.directusCgMarketCollectionName, redactedData)
    }
  } else {
    handleError(`cgMarketCoinsToSQL: cgMarketCoins is empty`)
  }

  return
}

export async function executeAllCoingeckoOperations() {
  // cv.cgToggleFav && (await cgFavoriteCoinsToSQL())
  cv.cgToggleMarketSQL && (await cgMarketCoinsToSQL())
  // cv.cgToggleMarketMongoDB && (await cgMarketCoinsToMongoDB())
}

/**
 * CMC
 */

export async function fetchCmcListingsLatest() {
  console.info(`cmc:${++trackingNumber.cmc} - ${new Date().toISOString()}`)

  const apiKeyInfo = await fetchThenParse(
    [cv.cmcApiUrlWithVersion, `/key/info`],
    { 'X-CMC_PRO_API_KEY': cv.cmcApiKey }
  )
  if (!apiKeyInfo) {
    return false
  }

  const apiKeyUsage = apiKeyInfo?.data?.usage
  cv.nodeEnvIsDev && cijs(apiKeyUsage)

  if (
    !(
      apiKeyUsage?.current_minute?.requests_left > 15 &&
      apiKeyUsage?.current_month?.credits_left > 1000
    )
  ) {
    handleError(`CMC: Reaching maximum soft limit`)
    return false
  }

  const listingsLatest = await fetchThenParse(
    [
      cv.cmcApiUrlWithVersion,
      '/cryptocurrency/listings/latest',
      `?limit=${cv.cmcLimitPerPage}`,
    ],
    { 'X-CMC_PRO_API_KEY': cv.cmcApiKey }
  )

  if (cv.nodeEnvIsDev || cv.cmcLogGET) {
    isArrayWithData(listingsLatest?.data) && cijs(listingsLatest?.data?.at(-1))
  }

  return listingsLatest
}

export const cmcMarketToSQL = async () => {
  const cmcResponse = await fetchCmcListingsLatest()
  const cmcData = cmcResponse?.data as any[]

  if (!isArrayWithData(cmcData)) {
    handleError(`cmc data not existed`)
    return
  }

  // copy & sort again my market cap & true ranking
  const refinedData: any[] = cmcData
    .map((_coinObj: any) => {
      return {
        symbol: _coinObj.symbol?.toLowerCase(),
        name: _coinObj.name,
        // rank: _coinObj.cmc_rank,
        circulating_supply: Math.floor(_coinObj.circulating_supply),
        usd_price: _coinObj?.quote?.USD?.price,
        usd_volume_24h: Math.floor(_coinObj?.quote?.USD?.volume_24h),
        usd_market_cap: Math.floor(_coinObj?.quote?.USD?.market_cap),
        last_updated_str: _coinObj?.quote?.USD?.last_updated,
        last_updated_unix: Math.floor(
          +new Date(_coinObj?.quote?.USD?.last_updated) / 1000
        ),
      }
    })
    .sort((a: any, b: any) => b?.usd_market_cap - a?.usd_market_cap)
    .map((_v, _i) => {
      return { ..._v, rank: _i + 1 }
    })

  if (cv.nodeEnvIsDev || cv.cmcLogPOST) {
    cijs(refinedData.at(-1))
  }

  await insertToDirectus(cv.directusCmcCollectionName, refinedData)
}

/**
 * MongoDB
 */

// const client =
//   cv.mongoDbUri &&
//   new MongoClient(cv.mongoDbUri, {
//     // serverApi: ServerApiVersion.v1,
//   })

// process.on('exit', async () => {
//   if (client) {
//     // Ensures that the client will close when you finish/error
//     console.info(`Closing MongDB connection...`)
//     await client.close()
//   }
// })

// export async function cgMarketCoinsToMongoDB() {
//   if (client) {
//     const db = client.db('cg')
//     const collection = db.collection(cv.mongoDbCgCollectionName)
//     const data = await fetchCgMarketCoins()
//     if (isObjectWithData(data)) {
//       await collection.insertOne({ data })
//     }
//   }
// }

// export async function cmcToMongoDB() {
//   if (client) {
//     const mongoCmcDatabase = client.db('cmc')
//     const mongoCmcCollection = mongoCmcDatabase.collection(
//       cv.mongoDbCmcCollectionName
//     )
//     setInterval(async () => {
//       const listingsLatest = await fetchCmcListingsLatest()
//       if (isObjectWithData(listingsLatest)) {
//         await mongoCmcCollection.insertOne(listingsLatest)
//       }
//     }, cv.cmcTimeout)
//   }
// }
