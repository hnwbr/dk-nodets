import dotenv from 'dotenv'

dotenv.config()

export const nodeEnv = process.env.NODE_ENV?.toLocaleLowerCase() || ''
export const nodeEnvIsDev = nodeEnv?.includes(`dev`)
export const nodeEnvIsProd = nodeEnv?.includes(`prod`)

const apiLocalTestPort = 3462

export const server = {
  hostname: process.env.HOSTNAME || 'localhost',
  port: process.env.PORT || apiLocalTestPort,
}

// export const backendHost = process.env.BACKEND_HOST || ''
// export const backendUrl = process.env.BACKEND_URL || `https://${backendHost}`
// export const backendLoginPath = process.env.BACKEND_LOGIN_PATH || '/login'
// export const backendLoginUrl = `${backendUrl}${backendLoginPath}`
// export const backendUsername = process.env.BACKEND_USERNAME
// export const backendPassword = process.env.BACKEND_PASSWORD
// export const frontendUsername = process.env.FRONTEND_USERNAME
// export const frontendPassword = process.env.FRONTEND_PASSWORD

export const undiciFetchTimeout =
  Number(process.env.UNDICI_FETCH_TIMEOUT_MS) || 5000

/**
 * cg
 */

export const cgToggleFav = process.env.CG_TOGGLE_FAV?.toLowerCase() === 'true'
export const cgToggleMarketSQL =
  process.env.CG_TOGGLE_MARKET_SQL?.toLowerCase() === 'true'
export const cgToggleMarketMongoDB =
  process.env.CG_TOGGLE_MARKET_MONGODB?.toLowerCase() === 'true'
export const cgApiBaseUrl = process.env.CG_API_BASE_URL || ''
export const cgMarketCoinsPerPage =
  Number(process.env.CG_MARKET_COINS_PER_PAGE) || 100
export const cgMarketPages = Number(process.env.CG_MARKET_PAGES) || 1
export const cgTimeout = Number(process.env.CG_TIMEOUT_IN_MS) || 300000 // default = 5 minute

/**
 * cmc
 */

export const cmcToggleSQL = process.env.CMC_TOGGLE_SQL?.toLowerCase() === 'true'
export const cmcToggleMongoDB =
  process.env.CMC_TOGGLE_MONGODB?.toLowerCase() === 'true'
export const cmcApiBaseUrl = process.env.CMC_API_BASE_URL || ''
export const cmcApiVersion = process.env.CMC_API_VERSION || ''
export const cmcApiUrlWithVersion =
  process.env.CMC_API_URL_WITH_VERSION || `${cmcApiBaseUrl}/${cmcApiVersion}`
export const cmcApiKey = process.env.CMC_API_KEY || ''
export const cmcLimitPerPage = Number(process.env.CMC_LIMIT_PER_PAGE) || 200
export const cmcTimeout = Number(process.env.CMC_TIMEOUT_IN_MS) || 3600000 // default = 1 hour
export const cmcCron = process.env.CMC_CRON || '0 */4 * * *' // default at minute 0 every 4 hour
export const cmcLogGET = process.env.CMC_LOG_GET?.toLowerCase() === 'true'
export const cmcLogPOST = process.env.CMC_LOG_POST?.toLowerCase() === 'true'

/**
 * SQL
 */

export const directusBaseUrl = process.env.DIRECTUS_BASE_URL || ''
export const directusToken = process.env.DIRECTUS_BOSS_TOKEN
export const directusCgFavCollectionName =
  process.env.DIRECTUS_CG_FAV_COLLECTION_NAME || ''
export const directusCgMarketCollectionName =
  process.env.DIRECTUS_CG_MARKET_COLLECTION_NAME || ''
export const directusCmcCollectionName =
  process.env.DIRECTUS_CMC_COLLECTION_NAME || ''

/**
 * MongDB
 */

export const mongoDbUri = process.env.MONGODB_URI
export const mongoDbCgCollectionName =
  process.env.MONGODB_CG_COLLECTION_NAME || ''
export const mongoDbCmcCollectionName =
  process.env.MONGODB_CMC_COLLECTION_NAME || ''
