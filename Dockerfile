FROM node:lts-alpine
WORKDIR /app
COPY package.json .
RUN npm i
COPY . .
RUN npm run build
# RUN apk update && apk add eza
# CMD ["eza", "--help"]
# CMD ["eza", "-laT", "--group-directories-first", "--time-style=long-iso", "--level=2", "--color=always"]
# CMD ["eza", "-laT", "--group-directories-first", "--time-style=long-iso", "--git-ignore", "--level=3", "--color=always", "/"]
# CMD ["npm", "run", "try"]
# CMD ["npm", "start"]
CMD ["node", "./dist/main.js"]